# Hugo-random3

This is my Hugo theme, translated form my Pelican theme, which was an adaptation of Sam Hocevar's dev-random theme.

This Hugo theme is provided "as-is" by the author. You are kindly asked to use, fork, modify or twick it to fit your needs.
Please just keep my name as the original author and provide a link to my [website](https://www.22decembre.eu).

## installation

Clone the git repo, and write the theme's name in the conf' file and you're on tracks.

    cd ~/hugo/themes/
    git clone https://framagit.org/22decembre/hugo-random3
    
In config.toml :

    theme = "hugo-random3"

## extras

This theme has a few extras that you can configure through config.toml:

    [params]
    isso="https://www.your.website.net/isso/"
    liberapay="You"
    tipeee="you"
    spamtrap="honey@your.website.net"

[Isso](https://posativ.org/isso/) is a python self-hosted comment system.

If you provide [Tipeee](https://en.tipeee.com/) or [Liberapay](https://liberapay.com/) accounts name, buttons will show up to your accounts.

Please feel free to donate yourself: https://en.tipeee.com/22decembre

## languages

The theme is translated in English, French and Danish.

## Sidebar links.

The sidebar provide a blogroll listing, links to social networks and local links to your sites. If you wish to use them, you have to configure them in the data folder of your Hugo installation :

    cd ~/hugo/data/

Blogroll.json:

    {  "Cborne":
        {   "name": "Cyrille Borne",
            "url": "https://cborne.fr" },
            
        "Indre":
        { "name": "Indre's Tumblr",
        "url": "https://damage-girl.tumblr.com/" },
        
        ...
        
        "Authueil":
        { "name": "Authueil",
        "url": "https://authueil.fr/" }
    }
   
Social.json :

    { "Mastodon":
        {   "name": "Mastodon",
            "url": "https://mamot.fr/@you" },
        
        "Diaspora":
        { "name": "Diaspora",
        "url": "https://diasp.eu/u/you" }
    }
   
Local.json :

    { "Photos":
        {   "name": "Your photos",
            "url": "https://photos.your.website.net" }
    }

Logos of Mastodon, Facebook, Diaspora, Liberapay and Tipeee are just pictures and not tracking links.
